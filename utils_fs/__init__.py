from google.cloud import firestore


# #! Only need this if you're running this code locally.
# from dotenv import load_dotenv, find_dotenv
# load_dotenv(find_dotenv())
# #! Only need this if you're running this code locally.


db = firestore.Client()


def get_document_name(name_collection, name_document):
    print(f'Get document {name_document} in {name_collection}')
    data = {}
    doc = db.collection(name_collection).document(name_document).get()
    data = doc.to_dict()
    return data


def get_document_filter(name_collection, name_filter, value_filter, value_operator):
    print(f'Get document {name_filter}|{value_filter} in {name_collection}')
    data = []
    docs = db.collection(name_collection).where(
        name_filter, value_operator, value_filter).get()
    for doc in docs:
        data.append(doc.to_dict())
    return data


# print(get_document_filter('test_trusted_zone', 'id', [10, 20, 1], 'in'))
