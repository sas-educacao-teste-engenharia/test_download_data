from flask import jsonify
import jwt
import os
from utils_fs import get_document_name, get_document_filter

# #! Only need this if you're running this code locally.
# from dotenv import load_dotenv, find_dotenv
# load_dotenv(find_dotenv())
# #! Only need this if you're running this code locally.

#! Environ
SECRET_KEY = os.environ.get('SECRET_KEY')


name_collection = 'test_trusted_zone'


def get_data(data_json):
    id_document = data_json.get("id_document")
    name_filter = data_json.get("name_filter")
    value_filter = data_json.get("value_filter")
    value_operator = data_json.get("value_operator")
    data = {}

    if (id_document == None) and (name_filter == None):
        return (401, {'error': 'Not filter'})

    elif ((id_document != None) and (name_filter != None)) or (id_document != None):
        data = get_document_name(name_collection, str(id_document).zfill(12))
        return (200, data)

    elif(name_filter != None):

        if value_filter != None and value_operator != None:
            data = get_document_filter(name_collection, name_filter,
                                       value_filter, value_operator)
            return (200, data)
        else:
            return (401, {'error': 'Missing operator or filter'})


def test_download_data(request):
    """Function main

    Args:
        request: Request by user

    Returns:
        request_response (json): {
            "id_document": string or integer [optional],
            "name_filter": String [optional], 
            "value_filter": String, number or array [optional], 
            "value_operator": Query -> ["<", "<=", "==", ">", ">=", "array-contains", "in"] [optional]
        }
    """
    print(f'Request: {request}')

    # CORS
    headers = {
        'Access-Control-Allow-Origin': '*'
    }

    # Method OPTIONS
    if request.method == 'OPTIONS':
        headers.update({
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600',
            'Content-Type': 'application/json'
        })
        return ('', 204, headers)

    # Method POST
    if request.method == 'POST':
        # Read data sent json
        data_request = request.get_json()

        if data_request != None:

            secret_key = None
            try:
                # Decode key
                secret_key = jwt.decode(
                    request.headers['Authorization'].split(" ")[1],
                    'secret',
                    algorithms=['HS256']
                )
            except Exception as erro:
                print(f'ERRO: {erro}')
                data_send = {
                    'error': 'Please, inside the correct secret key.'
                }
                return (jsonify(data_send), 401, headers)

            # Test secret key
            if secret_key.get('key') == SECRET_KEY:
                # Get data
                status, data = get_data(data_request)

                return (jsonify(data), status, headers)
            else:
                data_send = {
                    'error': 'Please, inside the correct secret key.'
                }
                return (jsonify(data_send), 401, headers)
        return (jsonify({'error': 'Missing data'}), 401, headers)

    return ('Method not exist, please try POST method', 405, headers)
